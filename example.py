import numpy as np

lemonad = 1
rain = 1
friend = 1

def activation_function(x):
    if x >= 0.5:
        return 1
    return 0

def predict(lemonad, rain, friend):
    print("input: " + str([lemonad, rain, friend]))
    inputs = np.array([lemonad, rain, friend])
    weights_input_to_hiden_1 = [0.25, 0.25, 0]
    weights_input_to_hiden_2 = [0.5, -0.4, 0.9]
    weights_input_to_hiden = np.array([weights_input_to_hiden_1, weights_input_to_hiden_2])
    weights_hiden_to_output = np.array([-1, 1])

    hiden_input = np.dot(weights_input_to_hiden, inputs)
    print("hidden input: " + str(hiden_input))

    hiden_output = np.array([activation_function(x) for x in hiden_input])
    print("hiden_output: " + str(hiden_output))

    output = np.dot(weights_hiden_to_output, hiden_output)
    print("output: " + str(output))

    return  activation_function(output) == 1

print("result: " + str(predict(lemonad, rain, friend)))